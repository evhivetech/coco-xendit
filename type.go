package cocoxendit

// Request Payload
type (
	RequestCreateInvoice struct {
		ExternalID  string `json:"external_id"`
		PayerEmail  string `json:"payer_email"`
		Description string `json:"description"`
		Amount      int64  `json:"amount"`
	}

	RequestSanboxCreateInvoice struct {
		ExternalID  string `json:"external_id"`
		PayerEmail  string `json:"payer_email"`
		Description string `json:"description"`
		Amount      int64  `json:"amount"`
		Account     string `json:"account"`
	}
)

// Response Payload
type (
	ResponseInvoice struct {
		ID                        string         `json:"id"`
		ExternalID                string         `json:"external_id"`
		UserID                    string         `json:"user_id"`
		Status                    string         `json:"status"`
		MerchantName              string         `json:"merchant_name"`
		MerchantProfilePictureURL string         `json:"merchant_profile_picture_url"`
		Amount                    int64          `json:"amount"`
		PayerEmail                string         `json:"payer_email"`
		Description               string         `json:"description"`
		ExpiryDate                string         `json:"expiry_date"`
		InvoiceURL                string         `json:"invoice_url"`
		AvailableBanks            []ResponseBank `json:"available_banks"`
		ShouldExcludeCreditCard   bool           `json:"should_exclude_credit_card"`
		ShouldSendEmail           bool           `json:"should_send_email"`
		Created                   string         `json:"created"`
		Updated                   string         `json:"updated"`
	}
)

// Additional Type
type (
	ResponseBank struct {
		BankCode          string `json:"bank_code"`
		CollectionType    string `json:"collection_type"`
		BankAccountNumber string `json:"bank_account_number"`
		TransferAmount    int64  `json:"transfer_amount"`
		BankBranch        string `json:"bank_branch"`
		AccountHolderName string `json:"account_holder_name"`
		IdentityAmount    int64  `json:"identity_amount"`
	}
)

// Error Type
type (
	ErrorInvoice struct {
		ErrorCode string `json:"error_code"`
		Message   string `json:"message"`
	}
)
