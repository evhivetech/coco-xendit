package cocoxendit

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// GetInvoice - Get Invoice by ID
func GetInvoice(invoiceID string, bookingType string) (*ResponseInvoice, error) {
	var (
		invoice ResponseInvoice
		apiKey  string
		token   string

		client = GetClient()
		url    = "https://api.xendit.co/v2/invoices/" + invoiceID
	)

	if bookingType == "Private Office" {
		apiKey = os.Getenv("XENDIT_API_KEY") + ":"
	} else if bookingType == "Coliving" {
		apiKey = os.Getenv("XENDIT_API_KEY_COLIVING") + ":"
	}
	token = base64.StdEncoding.EncodeToString([]byte(apiKey))

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", "Basic "+token)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("cache-control", "no-cache")

	res, err := client.Do(req)
	if err != nil {
		return nil, errors.New("failed to get response from xendit")
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

	err = json.Unmarshal(body, &invoice)
	if err != nil {
		fmt.Println(err)
		return nil, errors.New("failed to unmarshal response, please contact xendit for body response")
	}

	return &invoice, nil
}
