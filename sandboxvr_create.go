package cocoxendit

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

//CreateSandboxInvoice - create invoice for the sandboxvr
func CreateSandboxInvoice(payload RequestCreateInvoice) (ResponseInvoice, error) {

	var (
		apiKey, token string
		invoice       ResponseInvoice
	)

	const url = "https://api.xendit.co/v2/invoices"

	//add the env
	apiKey = os.Getenv("XENDIT_SANDBOX_API_KEY") + ":"
	token = base64.StdEncoding.EncodeToString([]byte(apiKey))

	bodyReq, err := json.Marshal(payload)
	if err != nil {
		return invoice, errors.New("failed convert request to json, please check request payload")
	}

	//requesr
	req, _ := http.NewRequest("POST", url, strings.NewReader(string(bodyReq)))
	req.Header.Add("Authorization", "Basic "+token)
	req.Header.Add("Content-Type", "application/json")

	res, err := GetClient().Do(req)
	if err != nil {
		return invoice, errors.New("failed to get response from xendit")
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

	err = json.Unmarshal(body, &invoice)
	if err != nil {
		fmt.Println(err)
		return invoice, errors.New("failed to unmarshal response, please contact xendit for body response")
	}

	return invoice, nil

}
