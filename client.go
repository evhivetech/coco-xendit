package cocoxendit

import (
	"net"
	"net/http"
	"sync"
	"time"
)

var client *http.Client
var once sync.Once

// GetClient - Setting
func GetClient() *http.Client {
	once.Do(func() {
		client = &http.Client{
			Transport: &http.Transport{
				DialContext: (&net.Dialer{
					Timeout:   10 * time.Second,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   10 * time.Second,
				ResponseHeaderTimeout: 10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		}
	})

	return client
}
